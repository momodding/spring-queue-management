package com.momodding.rabbitadmin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class RabbitAdminApplication

fun main(args: Array<String>) {
    runApplication<RabbitAdminApplication>(*args)
}
