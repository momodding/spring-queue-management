package com.momodding.rabbitadmin.controller

import com.momodding.rabbitadmin.service.IQueueService
import com.momodding.rabbitadmin.service.MessageListener
import org.springframework.amqp.core.BindingBuilder
import org.springframework.amqp.core.DirectExchange
import org.springframework.amqp.core.Queue
import org.springframework.amqp.core.QueueBuilder
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(value = ["/queues"], produces = ["application/json"])
class QueueController constructor(
    private val iQueueService: IQueueService
) {

    @PostMapping("add/{id}")
    fun addQueue(
        @PathVariable id: String
    ): ResponseEntity<Any> {

        val queue: Queue = QueueBuilder
            .durable("loan_pending_$id")
            .deadLetterExchange("loan_pending_$id.dlx")
            .deadLetterRoutingKey("loan_pending_$id")
            .build()

        val exchange: DirectExchange = DirectExchange("loan_pending_$id.direct").apply {
            isDelayed = true
            addArgument("x-delayed-type", "direct")
        }

        val binding = BindingBuilder.bind(queue).to(exchange).with("loan_pending_$id.route")

        iQueueService.createQueue(queue, binding, exchange)

        val response = mapOf(
            "status" to "OK",
            "data" to true,
            "message" to "Success"
        )
        return ResponseEntity(response, HttpStatus.OK)
    }

    @PostMapping("{id}/push")
    fun pushMessage(
        @PathVariable id: String,
        @RequestBody request: Map<String, Any>
    ): ResponseEntity<Any> {
        iQueueService.pushQueue("loan_pending_$id", request)

        val response = mapOf(
            "status" to "OK",
            "data" to true,
            "message" to "Success"
        )
        return ResponseEntity(response, HttpStatus.OK)
    }

    @PostMapping("{id}/listener")
    fun addQueueToListener(
        @PathVariable id: String
    ): ResponseEntity<Any> {
        iQueueService.addQueueToListener(MessageListener.MESSAGE_LISTENER_ID, "loan_pending_$id")

        val response = mapOf(
            "status" to "OK",
            "data" to true,
            "message" to "Success"
        )
        return ResponseEntity(response, HttpStatus.OK)
    }

    @DeleteMapping("{id}/listener")
    fun removeQueueFromListener(
        @PathVariable id: String
    ): ResponseEntity<Any> {
        iQueueService.removeQueueFromListener(MessageListener.MESSAGE_LISTENER_ID, "loan_pending_$id")

        val response = mapOf(
            "status" to "OK",
            "data" to true,
            "message" to "Success"
        )
        return ResponseEntity(response, HttpStatus.OK)
    }

    @GetMapping("{id}/listener/exist")
    fun isQueueExistOnListener(
        @PathVariable id: String
    ): ResponseEntity<Any> {
        val result = iQueueService.isQueueExistOnListener(MessageListener.MESSAGE_LISTENER_ID, "loan_pending_$id")

        val response = mapOf(
            "status" to "OK",
            "data" to result,
            "message" to "Success"
        )
        return ResponseEntity(response, HttpStatus.OK)
    }

    @GetMapping("listeners")
    fun queueListOnListener(): ResponseEntity<Any> {
        val result = iQueueService.queueListOnListener(MessageListener.MESSAGE_LISTENER_ID)

        val response = mapOf(
            "status" to "OK",
            "data" to result,
            "message" to "Success"
        )
        return ResponseEntity(response, HttpStatus.OK)
    }

    @GetMapping("{id}/exist")
    fun isQueueExist(@PathVariable id: String): ResponseEntity<Any> {
        val result = iQueueService.isQueueExist("loan_pending_$id")

        val response = mapOf(
            "status" to "OK",
            "data" to result,
            "message" to "Success"
        )
        return ResponseEntity(response, HttpStatus.OK)
    }

    @GetMapping("{id}/count")
    fun countMessage(@PathVariable id: String): ResponseEntity<Any> {
        val result = iQueueService.countMessage("loan_pending_$id")

        val response = mapOf(
            "status" to "OK",
            "data" to result,
            "message" to "Success"
        )
        return ResponseEntity(response, HttpStatus.OK)
    }
}
