package com.momodding.rabbitadmin.service

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.amqp.core.Binding
import org.springframework.amqp.core.Exchange
import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.core.RabbitAdmin
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.amqp.rabbit.listener.AbstractMessageListenerContainer
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistry
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class QueueService @Autowired constructor(
    private val rabbitTemplate: RabbitTemplate,
    private val rabbitAdmin: RabbitAdmin,
    private val rabbitListenerEndpointRegistry: RabbitListenerEndpointRegistry,
) : IQueueService {
    override fun pushQueue(queueName: String, data: Any) {
        rabbitTemplate.convertAndSend(queueName, jacksonObjectMapper().writeValueAsString(data))
    }

    override fun createQueue(queue: Queue, binding: Binding?, exchange: Exchange?) {
        rabbitAdmin.declareQueue(queue)
        exchange?.let { rabbitAdmin.declareExchange(it) }
        binding?.let { rabbitAdmin.declareBinding(it) }
    }

    override fun addQueueToListener(listenerId: String, queueName: String) {
        if (isQueueExistOnListener(listenerId, queueName)) return
        val listenerContainer =
            rabbitListenerEndpointRegistry.getListenerContainer(listenerId) as AbstractMessageListenerContainer
        listenerContainer.addQueueNames(queueName)
    }

    override fun removeQueueFromListener(listenerId: String, queueName: String) {
        if (!isQueueExistOnListener(listenerId, queueName)) return
        val listenerContainer =
            rabbitListenerEndpointRegistry.getListenerContainer(listenerId) as AbstractMessageListenerContainer
        listenerContainer.removeQueueNames(queueName)
    }

    override fun isQueueExistOnListener(listenerId: String, queueName: String): Boolean {
        val listenerContainer =
            rabbitListenerEndpointRegistry.getListenerContainer(listenerId) as AbstractMessageListenerContainer
        return listenerContainer.queueNames.any { it.equals(queueName, true) }
    }

    override fun queueListOnListener(listenerId: String): List<String> {
        val listenerContainer =
            rabbitListenerEndpointRegistry.getListenerContainer(listenerId) as AbstractMessageListenerContainer
        return listenerContainer.queueNames.toList()
    }

    override fun isQueueExist(queueName: String): Boolean = rabbitTemplate.execute { channel ->
        try {
            channel.queueDeclarePassive(queueName)
        } catch (ex: Exception) {
            null
        }
    } != null

    override fun countMessage(queueName: String): Int {
        val queueProperties = rabbitAdmin.getQueueProperties(queueName)
        return try {
            queueProperties["QUEUE_MESSAGE_COUNT"].toString().toInt()
        } catch (ex: Exception) {
            0
        }
    }
}
