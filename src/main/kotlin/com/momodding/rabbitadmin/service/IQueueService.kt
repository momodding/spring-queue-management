package com.momodding.rabbitadmin.service

import org.springframework.amqp.core.Binding
import org.springframework.amqp.core.Exchange
import org.springframework.amqp.core.Queue

interface IQueueService {
    fun pushQueue(queueName: String, data: Any)

    fun createQueue(queue: Queue, binding: Binding? = null, exchange: Exchange? = null)

    fun addQueueToListener(listenerId: String, queueName: String)

    fun removeQueueFromListener(listenerId: String, queueName: String)

    fun isQueueExistOnListener(listenerId: String, queueName: String): Boolean

    fun queueListOnListener(listenerId: String): List<String>

    fun isQueueExist(queueName: String): Boolean

    fun countMessage(queueName: String): Int
}
