package com.momodding.rabbitadmin.service

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.amqp.core.Message
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Component

@Component
class MessageListener {

    @RabbitListener(id = MESSAGE_LISTENER_ID)
    fun messageListener(message: Message) {
        val body = message.body
        val timestamp = message.messageProperties.timestamp
        println("posted at: $timestamp with body: ${jacksonObjectMapper().writeValueAsString(body)}")
    }

    companion object {
        const val MESSAGE_LISTENER_ID = "messageListenerClass"
    }
}
