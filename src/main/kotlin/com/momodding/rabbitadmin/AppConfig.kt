package com.momodding.rabbitadmin

import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.amqp.rabbit.core.RabbitAdmin
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class AppConfig @Autowired constructor(
    private val connectionFactory: ConnectionFactory
) {

    @Bean
    fun rabbitAdmin(): RabbitAdmin = RabbitAdmin(connectionFactory)
}
